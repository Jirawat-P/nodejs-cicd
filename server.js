'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
    res.send('Hello openshift!\n  <p>New line added</p> <p>blahblahblah</p> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vestibulum lorem ligula, a vestibulum quam sodales in. Nullam non aliquam eros, dapibus vulputate felis. Suspendisse a ultrices mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc auctor malesuada ex gravida suscipit. Quisque lacus velit, rutrum sed pulvinar eget, ornare eu sapien. Maecenas malesuada sem ut orci molestie porta.</p>');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);